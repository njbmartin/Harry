//
//  AppDelegate.h
//  DynamicWizardApp
//
//  Created by Nico on 06/03/2016.
//  Copyright © 2016 Nico. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

