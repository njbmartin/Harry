//
//  ViewController.m
//  DynamicWizardApp
//
//  Created by Nico on 06/03/2016.
//  Copyright © 2016 Nico. All rights reserved.
//

#import "WizardController.h"

@interface WizardController ()

@end

@implementation WizardController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
        _currentPage = 0;
    UIViewController *coming = [_viewControllers objectAtIndex:0];
    
    [_dynamicUIView insertSubview: coming.view atIndex:0];
    [self setTitleBar];
    
}


-(void)setTitleBar {
    
    _BarTitle.title = [NSString stringWithFormat:@"Page: %d / %lu", _currentPage+1, (unsigned long)_viewControllers.count];

    if(_currentPage == 0)
    {
        _BarButtonItemLeft.title = @"Close";
    }else{
        _BarButtonItemLeft.title = @"Back";
    }
    
    if(_viewControllers.count-1 > _currentPage)
    {
        _BarButtonItemRight.title = @"Next";
    }else{
        _BarButtonItemRight.title = @"Done";
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)BarButtonItemLeftClick:(id)sender {
    if(_currentPage == 0){
        [self dismissView];
        return;
    }
    
    
    [UIView beginAnimations:@"View Flip" context:nil];
    [UIView setAnimationDuration:.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    UIViewController *coming = nil;
    UIViewController *going = nil;
    UIViewAnimationTransition transition;
    
    coming = [_viewControllers objectAtIndex:_currentPage - 1];
    going = [_viewControllers objectAtIndex:_currentPage];
    transition = UIViewAnimationTransitionNone;
    
    [UIView setAnimationTransition: transition forView:_dynamicUIView cache:YES];
    [coming viewWillAppear:YES];
    [going viewWillDisappear:YES];
    [going.view removeFromSuperview];
    [_dynamicUIView insertSubview: coming.view atIndex:0];
    [going viewDidDisappear:YES];
    [coming viewDidAppear:YES];
    
    [UIView commitAnimations];
    _currentPage--;
    [self setTitleBar];
    
}

-(void)dismissView {
    [self dismissViewControllerAnimated:TRUE completion:nil];
    _currentPage =0;
    _viewControllers = nil;
    return;
    
}

-(void)showSequence{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Views Displayed"
                                                                             message:[_viewColors description]
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    //We add buttons to the alert controller by creating UIAlertActions:
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action)
                               {
                                   [self dismissView];
                               }];
    [alertController addAction:actionOk];
    [self presentViewController:alertController animated:YES completion:nil];
    //[self dismissView];
    
}

- (IBAction)BarButtonItemRightClick:(id)sender {
    
    // We're done
    if(_currentPage >= _viewControllers.count - 1)
    {
        [self showSequence];
        
        return;
    }
    

    UIViewController *coming = nil;
    UIViewController *going = nil;
    
        coming = [_viewControllers objectAtIndex:_currentPage + 1];
        going = [_viewControllers objectAtIndex:_currentPage];


    [coming viewWillAppear:YES];
    [going viewWillDisappear:YES];
    [going.view removeFromSuperview];
    [_dynamicUIView insertSubview: coming.view atIndex:0];
    [going viewDidDisappear:YES];
    [coming viewDidAppear:YES];
    
    _currentPage++;
    [self setTitleBar];
}
@end
