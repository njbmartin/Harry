//
//  ViewController.h
//  DynamicWizardApp
//
//  Created by Nico on 06/03/2016.
//  Copyright © 2016 Nico. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *OpenWizardBtn;

@property(nonatomic, assign) int totalPages;
@property(nonatomic, strong) NSMutableArray *viewControllers;
@property(nonatomic, strong) NSMutableArray *viewColors;

- (IBAction)OpenWizardClick:(id)sender;


@end
