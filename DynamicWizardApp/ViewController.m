//
//  ViewController.m
//  DynamicWizardApp
//
//  Created by Nico on 06/03/2016.
//  Copyright © 2016 Nico. All rights reserved.
//

#import "ViewController.h"
#import "WizardController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(int)getRandomNumberBetween:(int)from to:(int)to {
    
    return (int)from + arc4random() % (to-from+1);
}

- (IBAction)OpenWizardClick:(id)sender {
    _viewControllers = [[NSMutableArray alloc] initWithCapacity:0];
    _viewColors = [[NSMutableArray alloc] initWithCapacity:0];

    NSDictionary *colors = [NSDictionary
                            dictionaryWithObjects: [NSArray arrayWithObjects:[UIColor redColor], [UIColor greenColor], [UIColor blueColor],[UIColor brownColor],[UIColor yellowColor], nil]
                            forKeys: [NSArray arrayWithObjects:@"Red", @"Green", @"Blue",@"Brown",@"Yellow", nil]];
    
    NSMutableArray *colorKeys = [NSMutableArray arrayWithArray:[colors allKeys]];
    for (NSUInteger i = colorKeys.count; i > 1; i--) [colorKeys exchangeObjectAtIndex:i - 1 withObjectAtIndex:arc4random_uniform((u_int32_t)i)];
    
    for(int i = 0; i < [self getRandomNumberBetween:1 to:5]; i++)
    {
        UIViewController *vc = [[UIViewController alloc] init];
        vc.view.backgroundColor = colors[colorKeys[i]];
        
        UILabel *testLabel =[[UILabel alloc] initWithFrame:CGRectMake(220, 50, 130, 80)];
        testLabel.backgroundColor = [UIColor clearColor];
        testLabel.textColor = [UIColor blackColor];
        
        // UIColor to NSString
        testLabel.text = colorKeys[i];
        [vc.view addSubview:testLabel];
        [_viewColors addObject:colorKeys[i]];
        [_viewControllers addObject:vc];
    }
    
    [self performSegueWithIdentifier:@"WizardView" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"WizardView"]) {
        // Get destination view controller and don't forget
        // to cast it to the right class
        WizardController *wizardController = [segue destinationViewController];
        // Pass data
        wizardController.viewColors= _viewColors;
        wizardController.viewControllers = _viewControllers;
    }
}


@end
