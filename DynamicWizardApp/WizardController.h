//
//  ViewController.h
//  DynamicWizardApp
//
//  Created by Nico on 06/03/2016.
//  Copyright © 2016 Nico. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WizardController : UIViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem *BarButtonItemLeft;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *BarButtonItemRight;
@property (weak, nonatomic) IBOutlet UINavigationItem *BarTitle;
@property (weak, nonatomic) IBOutlet UIView *dynamicUIView;
- (IBAction)BarButtonItemLeftClick:(id)sender;
- (IBAction)BarButtonItemRightClick:(id)sender;

@property(nonatomic, assign) int totalPages;
@property(nonatomic, assign) int currentPage;

@property(nonatomic, strong) NSMutableArray *viewColors;
@property(nonatomic, strong) NSMutableArray *viewControllers;
@end

